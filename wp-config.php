<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'fserving');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');
define( 'WPCF7_VALIDATE_CONFIGURATION', false );
define( 'WPCF7_USE_REALLY_SIMPLE_CAPTCHA', true );
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Dkc0i+AbZ.}}58z_5,h]2#[1-x}]b(K2Kg/RD2`Mi>?-qzVCa-_VTBtyYrp.[7+H');
define('SECURE_AUTH_KEY',  'DM,,_7:L?2E&b(Wq2F6,}=#z`:Xp}PuCRm!dhpj_8AGoUuGBmgWtahH,xl:%%k/r');
define('LOGGED_IN_KEY',    'LNGep-W|K^bj~8MRNb{e]L1?}44zP[DkV]$5{WCB>P+ v0vlwuvsw@2Qmgix;Xwr');
define('NONCE_KEY',        '@MTt~wf;+ih:8Agz9?AH2[`cZIY)v&:*u}Jny?9$)=%`mM`G0HW76[]y0g,>Ifxu');
define('AUTH_SALT',        '^bC~ayDJu=p [<0$zf9RU:b&EHAfpgc;(~<kQ<BgginlD>_3[TdqdG7s`WY=v?)5');
define('SECURE_AUTH_SALT', 't5;aD o.!i6oj[H,i9@NhH}rmRL0VE^ZDm4NqV9 ga>gx3CF^|F%q()LA! wQ`(r');
define('LOGGED_IN_SALT',   ';)K7!zNaT/3PP0._zZwH[bh-TK@JuvExr9$Fei],3.}0dV1W7mL(6?b$5VWH;R;J');
define('NONCE_SALT',       '.u|ZWN8J*2HB!9D(N>idYbuZAPO+wr-W40T<c`3<zLNZ|xDcoh%%/w01IBU%vGVT');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
