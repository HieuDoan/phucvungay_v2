<?php
get_header(); ?>
<div id="primary" class="">

	<main id="main" class="site-main" role="main">
		<?php
		get_template_part('template-parts/common/content', 'banner');
		get_template_part( 'template-parts/page/content', 'information' );
		get_template_part( 'template-parts/common/content', 'product' );
		get_template_part( 'template-parts/common/content', 'news' );
		get_template_part( 'template-parts/page/content', 'process' );
		get_template_part( 'template-parts/common/content', 'readme' );
		get_template_part( 'template-parts/common/content', 'application' );
		?>
	</main>
</div>
<?php get_footer();
?>
