<?php
/********************************************************************
// Get Theme Options
********************************************************************/
function ddev_get_option( $name ) {
	$ddev_options = get_option( 'ddev_options' );
	if( !empty( $ddev_options[$name] ))
		return $ddev_options[$name];

	return false ;
}

function ddev_get_setting( $name ) {
	$ddev_settings = get_option( 'ddev_settings' );
	if( !empty( $ddev_settings[$name] ))
		return $ddev_settings[$name];

	return false ;
}

/********************************************************************
// Add Post Thumbnails
********************************************************************/
add_theme_support( 'post-thumbnails' );

/********************************************************************
// Unset Image Sizes
********************************************************************/
function unset_image_sizes($sizes){
  unset( $sizes['thumbnail']);
  unset( $sizes['medium']);
  unset( $sizes['medium_large']);
  unset( $sizes['large']);
  return $sizes;
}
add_filter('intermediate_image_sizes_advanced', 'unset_image_sizes' );

/********************************************************************
// Remove Tag Width & Height in Post
********************************************************************/
function ddev_remove_width_attribute( $html ) {
	$html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
	return $html;
}
add_filter( 'post_thumbnail_html', 'ddev_remove_width_attribute', 10 );
add_filter( 'image_send_to_editor', 'ddev_remove_width_attribute', 10 );

/********************************************************************
// Config Excerpt Length
********************************************************************/
function ddev_custom_excerpt_length( $length ) {
	return 300;
}
add_filter( 'excerpt_length', 'ddev_custom_excerpt_length', 999 );

/********************************************************************
// Global Excerpt Function
********************************************************************/
function ddev_excerpt( $limit ) {
	$excerpt = explode(' ', get_the_excerpt(), $limit);
	if (count($excerpt) >= $limit) {
		array_pop($excerpt);
		$excerpt = implode(" ",$excerpt).'[...]';
	} else {
		$excerpt = implode(" ",$excerpt);
	}
	$excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
	return $excerpt.' [...]';
}

/********************************************************************
// Allow Upload Flash Files
********************************************************************/
function ddev_add_custom_mime_types($mimes){
	return array_merge($mimes,array (
		'swf' => 'application/x-shockwave-flash',
	));
}
add_filter('upload_mimes','ddev_add_custom_mime_types');

/********************************************************************
// Allow HTML Tags in Widget Title
********************************************************************/
function ddev_html_widget_title( $var) {
	$var = (str_replace( '[', '<', $var ));
	$var = (str_replace( ']', '>', $var ));
	$var = (str_replace( '__', '"', $var ));

	return $var ;
}
add_filter( 'widget_title', 'ddev_html_widget_title' );

/********************************************************************
// Register Short Code Url
********************************************************************/
add_filter('widget_text', 'do_shortcode');
// [url_base]
function ddev_url_base() {
	return get_bloginfo( "url" );
}
add_shortcode('url_base', 'ddev_url_base');

/********************************************************************
// Register My Menu in Theme
********************************************************************/
function ddev_register_my_menus() {
	register_nav_menus(
	array(
		'main_nav' => 'Main Nav',
		'footer_nav' => 'Footer Nav',
		)
	);
}
add_action( 'init', 'ddev_register_my_menus' );

/********************************************************************
// Register Sidebar
********************************************************************/
if (function_exists('register_sidebar')) {
	register_sidebar(array(
		'name'=> 'ADS Top',
		'id' => 'sidebar_ads_top',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => ''
	));
	register_sidebar(array(
		'name'=> 'Hot News',
		'id' => 'sidebar_hot_news',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => ''
	));
	register_sidebar(array(
		'name'=> 'Hot Category',
		'id' => 'sidebar_hot_category',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => ''
	));
	register_sidebar(array(
		'name'=> 'Sidebar Left',
		'id' => 'sidebar_left',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => ''
	));
	register_sidebar(array(
		'name'=> 'Sidebar Right',
		'id' => 'sidebar_right',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => ''
	));
	register_sidebar(array(
		'name'=> 'Downloads',
		'id' => 'sidebar_downloads',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => ''
	));
	
}

/********************************************************************
// Local PHP Date
********************************************************************/
function ddev_date( $local ) {
	if ($local == 'vi') {
		$nameday = date("l");
		
		switch ($nameday) {
			case "Saturday":
				$nameday="Thứ bảy";
				break;
			case "Sunday":
				$nameday="Chủ nhật";
				break;
			case "Monday":
				$nameday="Thứ hai";
				break;
			case "Tuesday":
				$nameday="Thứ ba";
				break;
			case "Wednesday":
				$nameday="Thứ tư";
				break;
			case "Thursday":
				$nameday="Thứ năm";
				break;
			case "Friday":
				$nameday="Thứ sáu";
				break;
		}
	echo "$nameday, ".date('d/m/Y');
	} else {
		echo date('D, d/m/Y');
	}
}

/********************************************************************
// Built SEO Titles
********************************************************************/
function ddev_seotitles() {
	if (is_tag()) {
		print wp_title('Bài viết theo Tags: ', true).' - '.get_bloginfo('name');
	} elseif (is_search()) {
		print 'Kết quả tìm kiếm: ';
	the_search_query();
		print ' - '.get_bloginfo('name');
	} elseif (is_404()) {
		print 'Page not found!';
	} elseif (is_home()) {
		print get_bloginfo('name').' | '.get_bloginfo('description');
	} else {
		$wptitle = wp_title('', false);
		$wptitle = str_replace('[Không sửa &#8211; xóa] &#8211; ', "", $wptitle);
		print $wptitle.' - '.get_bloginfo('name');
	}
}


/********************************************************************
// Post Views
********************************************************************/
function ddev_setpostviews( $post_id ) {
  $count_key ='ddev-views';
  $count = get_post_meta($post_id, $count_key, true);
  if($count == ''){
    $count = 0;
    delete_post_meta($post_id, $count_key);
    add_post_meta($post_id, $count_key, '0');
  } else {
    $count++;
    update_post_meta($post_id, $count_key, $count);
  }
}

function ddev_getpostviews( $post_id ) {
  $count_key ='ddev-views';
  $count = get_post_meta($post_id, $count_key, true);
  if($count == ''){
    delete_post_meta($post_id, $count_key);
    add_post_meta($post_id, $count_key, '0');
    return "0";
  }
  return $count;
}

/********************************************************************
// Download Counter
********************************************************************/
function ddev_setdownloads( $post_id ) {
  $count_key ='ddev-downloads';
  $count = get_post_meta($post_id, $count_key, true);
  if($count == ''){
    $count = 0;
    delete_post_meta($post_id, $count_key);
    add_post_meta($post_id, $count_key, '0');
  } else {
    $count++;
    update_post_meta($post_id, $count_key, $count);
  }
}

function ddev_getdownloads( $post_id ) {
  $count_key ='ddev-downloads';
  $count = get_post_meta($post_id, $count_key, true);
  if($count == ''){
    delete_post_meta($post_id, $count_key);
    add_post_meta($post_id, $count_key, '0');
    return "0";
  }
  return $count;
}

/********************************************************************
// Get Link Img Post
********************************************************************/
function ddev_get_link_img_post() {
	global $post;
	preg_match_all('/src="(.*)"/Us',get_the_content(),$matches);
	$link_img_post = $matches[1];
	return $link_img_post;
}

/********************************************************************
// Check Link Thumbnail
********************************************************************/
function ddev_check_link_thumb( $post_id ) {
	$img_customfield = get_post_meta($post_id, 'thumb', true);
	$img_attachment_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
	
	// get thumbnail
	if ($img_customfield) {
		$link_thumb = $img_customfield;
	} elseif ($img_attachment_image) {
		$link_thumb = $img_attachment_image[0];
	} else {
		$link_thumb = "";
	}
	return $link_thumb;
}

/********************************************************************
// Get Link Thumbnail
********************************************************************/
function ddev_get_link_thumb( $post_id ) {
	$img_customfield = get_post_meta($post_id, 'thumb', true);
	$img_attachment_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
	$img_uploads = get_children( array('post_parent' => $post_id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => 'ASC', 'orderby' => 'menu_order ID', 'numberposts' => 1) );
	
	$post_content = get_post_field('post_content', $post_id);
	$img_post = preg_match_all('/\< *[img][^\>]*src *= *[\"\']{0,1}([^\"\'\ >]*)/',$post_content,$matches);
	$img_default = get_template_directory_uri().'/images/no-thumb.png';
	
	// get thumbnail
	if ($img_customfield) {
		$link_thumb = $img_customfield;
	} elseif ($img_attachment_image) {
		$link_thumb = $img_attachment_image[0];
	} elseif ($img_uploads == true) {
		foreach($img_uploads as $id => $attachment) {
			$img = wp_get_attachment_image_src($id, 'full');
			$link_thumb = $img[0];
		}
	} elseif (count($matches[1]) > 0) {
		$link_thumb = $matches[1][0];
	} else {
		$link_thumb = $img_default;
	}
	return $link_thumb;
}

/********************************************************************
// Show Thumbnail (no permalink to story, image only)
********************************************************************/
function ddev_thumb( $w,$h ) {
	global $post;
	$img_customfield = get_post_meta($post->ID, 'thumb', true);
	$title = get_the_title($post);
    $title = preg_replace("/<img[^>]+\>/i", "", $title); 
	$img_attachment_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
	$img_uploads = get_children( array('post_parent' => $post->ID, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => 'ASC', 'orderby' => 'menu_order ID', 'numberposts' => 1) );
	$img_post = preg_match_all('/\< *[img][^\>]*src *= *[\"\']{0,1}([^\"\'\ >]*)/',get_the_content(),$matches);
	$img_default = get_template_directory_uri().'/images/no-thumb.png';

	@define( BFITHUMB_UPLOAD_DIR, 'Thumbcache' );	
	$params = array( 'width' => $w, 'height' => $h );

	// get thumbnail
	if ($img_customfield) {
		echo '<img src="'. bfi_thumb( $img_customfield, $params ) .'" alt="'.$title.'" title="'.$title.'" />';
	} elseif ($img_attachment_image) {		
		echo '<img src="'. bfi_thumb( $img_attachment_image[0], $params ) .'" alt="'.$title.'" title="'.$title.'" />';
	} elseif ($img_uploads == true) {
		foreach($img_uploads as $id => $attachment) {
			$img = wp_get_attachment_image_src($id, 'full');
			echo '<img src="'. bfi_thumb( $img[0], $params ) .'" alt="'.$title.'" title="'.$title.'" />';
		}
	} elseif (count($matches[1]) > 0) {
		echo '<img src="'. bfi_thumb( $matches[1][0], $params ) .'" alt="'.$title.'" title="'.$title.'" />';
	} else {
		echo '<img src="'. bfi_thumb( $img_default, $params ) .'" alt="'.$title.'" title="'.$title.'" />';
	}
}


/********************************************************************
// Page Navigation
********************************************************************/
function ddev_pagenavi( $query = false, $num = false ){
	echo '<div class="ddev-pagenavi">';	
		ddev_get_pagenavi( $query, $num );
	echo '</div>';
}


function ddev_mce_buttons(){
	return $mce_buttons = array( 
		'bold',				// Applies the bold format to the current selection.
		'italic',			// Applies the italic format to the current selection.
		'strikethrough',	// Applies strike though format to the current selection.
		'bullist',			// Formats the current selection as a bullet list.
		'numlist',			// Formats the current selection as a numbered list.
		'blockquote',		// Applies block quote format to the current block level element.
		'hr',				// Inserts a horizontal rule into the editor.
		'alignleft',		// Left aligns the current block or image.
		'aligncenter',		// Left aligns the current block or image.
		'alignright',		// Right aligns the current block or image.
		'link',				// Creates/Edits links within the editor.
		'unlink',			// Removes links from the current selection.
		'wp_more',			// Inserts the <!-- more --> tag.
		'spellchecker',		// ???
		'wp_adv',			// Toggles the second toolbar on/off.
		'dfw' 				// Distraction-free mode on/off.
	);
}
add_filter("mce_buttons", "ddev_mce_buttons");

function ddev_mce_buttons_2(){
	return $mce_buttons_2 = array( 
		'formatselect',		// Dropdown list with block formats to apply to selection.
		'underline',		// Applies the underline format to the current selection.
		'alignjustify',		// Full aligns the current block or image.
		'forecolor',		// Applies foreground/text color to selection.
		'pastetext',		// Toggles plain text pasting mode on/off.
		'removeformat',		// Removes the formatting from the current selection.
		'charmap',			// Inserts custom characters into the editor.
		'outdent',			// Outdents the current list item or block element.
		'indent',			// Indents the current list item or block element.
		'undo',				// Undoes the last operation.
		'redo',				// Redoes the last undoed operation.
		'wp_help'			// Opens the help.
	);
}
add_filter("mce_buttons_2", "ddev_mce_buttons_2");

/********************************************************************
// Get File Size
********************************************************************/
function ddev_getRemoteFilesize($url, $formatSize = true, $useHead = true){
  if (false !== $useHead) {
    stream_context_set_default(array('http' => array('method' => 'HEAD')));
  }
  $head = array_change_key_case(get_headers($url, 1));
  // content-length of download (in bytes), read from Content-Length: field
  $clen = isset($head['content-length']) ? $head['content-length'] : 0;

  // cannot retrieve file size, return "-1"
  if (!$clen) {
    return -1;
  }

  if (!$formatSize) {
    return $clen; // return size in bytes
  }

  $size = $clen;
  switch ($clen) {
    case $clen < 1024:
        $size = $clen .' B'; break;
    case $clen < 1048576:
        $size = round($clen / 1024, 2) .' kB'; break;
    case $clen < 1073741824:
        $size = round($clen / 1048576, 2) . ' MB'; break;
    case $clen < 1099511627776:
        $size = round($clen / 1073741824, 2) . ' GB'; break;
  }
  
  return $size; // return formatted size
}


/********************************************************************
// Auto Save Img Post
********************************************************************/
class ddev_auto_save_images {
  function __construct(){     
    add_filter( 'content_save_pre',array($this,'post_save_images') ); 
  }
  function post_save_images( $content ){
    if( ($_POST['save'] || $_POST['publish'] )){
      set_time_limit(240);
      global $post;
      $post_id = $post->ID;
      $preg = preg_match_all('/<img.*?src="(.*?)"/',stripslashes($content),$matches);
      if($preg){
        foreach($matches[1] as $image_url){
          if(empty($image_url)) continue;
          $pos = strpos($image_url,$_SERVER['HTTP_HOST']);
          if($pos === false){
            $res = $this->save_images($image_url,$post_id);
            $replace = $res['url'];
            $content = str_replace($image_url,$replace,$content);
          }
        }
      }
    }
    remove_filter( 'content_save_pre', array( $this, 'post_save_images' ) );
    return $content;
  }
  function save_images($image_url,$post_id){
    $file = file_get_contents($image_url);
    $post = get_post($post_id);
    $posttitle = $post->post_title;
    $postname = sanitize_title($posttitle);
    $im_name = "$postname-$post_id.jpg";
    $res = wp_upload_bits($im_name,'',$file);
    $this->insert_attachment($res['file'],$post_id);
    return $res;
  }
  
  function insert_attachment($file,$id){
    $dirs = wp_upload_dir();
    $filetype = wp_check_filetype($file);
    $attachment = array(
      'guid' => $dirs['baseurl'].'/'._wp_relative_upload_path($file),
      'post_mime_type' => $filetype['type'],
      'post_title' => preg_replace('/\.[^.]+$/','',basename($file)),
      'post_content' => '',
      'post_status' => 'inherit'
    );
    $attach_id = wp_insert_attachment($attachment,$file,$id);
    $attach_data = wp_generate_attachment_metadata($attach_id,$file);
    wp_update_attachment_metadata($attach_id,$attach_data);
    return $attach_id;
  }
}
new ddev_auto_save_images();

/* Cơ cấu tổ chức */
function buildTree( $ar, $pid = null ) {
 $op = array();
 $i = 0;
  foreach( $ar as $item ) {
      $i++;
      if( $item->parent == $pid ) {
      $op[$i] = array(
		'term_id' => $item->term_id,
		'name' => $item->name,
		'slug' => $item->slug,
		'term_group' => $item->term_group,
		'term_taxonomy_id' => $item->term_taxonomy_id,
		'taxonomy' => $item->taxonomy,
		'description' => $item->description,
		'parent' => $item->parent,
		'count' => $item->count,
		'filter' => $item->filter,

	);
	$children =  buildTree( $ar, $item->term_id);
	if( $children ) {
				$op[$i]['children'] = $children;
			}
		}
	}
return $op;
}
/* End cơ cấu tổ chức */

/* Lịch công tác */
add_action( 'edit_form_after_title', 'changeshowEditor' );
function changeshowEditor() 
{
	global $post, $wp_meta_boxes;
    switch(get_post_type($post)){
        case 'lich-cong-tac':
			do_meta_boxes( get_current_screen(), 'normal', $post );
			unset( $wp_meta_boxes['post']['normal'] );
		break;
		default:
		break;
	}
}
add_filter( 'default_content', 'my_editor_content', 10, 2 );
function my_editor_content( $content, $post ) {
    switch( $post->post_type ) {
        case 'lich-cong-tac':
            $content = '
            	<table class="lichtuan">
					<tbody>
						<tr class="first">
							<td align="center" width="12%">Thứ/Ngày</td>
							<td align="center" width="44%">Buổi sáng</td>
							<td align="center" width="44%">Buổi chiều</td>
						</tr>
						<tr class="row1">
							<td class="text" rowspan="1">Thứ Hai</td>
							<td>
								<div class="noidung"></div>
							</td>
							<td></td>
						</tr>
						<tr class="row0">
							<td class="text" rowspan="1">Thứ Ba</td>
							<td>
								<div class="noidung"></div>
							</td>
							<td></td>
						</tr>
						<tr class="row1">
							<td class="text" rowspan="1">Thứ Tư</td>
							<td>
								<div class="noidung">
								</div>
							</td>
							<td></td>
						</tr>
						<tr class="row0">
							<td class="text" rowspan="1">Thứ Năm</td>
							<td>
								<div class="noidung">

								</div>
							</td>
							<td></td>
						</tr>
						<tr class="row1">
							<td class="text" rowspan="1">Thứ Sáu</td>
							<td>
								<div class="noidung"></div>
							</td>
							<td></td>
						</tr>
						<tr class="row0">
							<td class="text" rowspan="1">Thứ Bảy</td>
							<td>
								<div class="noidung"></div>
							</td>
							<td></td>
						</tr>
						<tr class="row1">
							<td class="text">Chủ nhật</td>
							<td></td>
							<td></td>
						</tr>
					</tbody>
				</table>
            ';
        break;
        default:
            $content = '';
        break;
    }
    return $content;
}

add_action('admin_head', 'my_custom_fonts_calendar');
function my_custom_fonts_calendar() {
	echo '<style>
		
	</style>';
}
/* End lịch công tác*/

/* Upload csv user */
include("csv/viettechkey-import-users-from-csv.php" );
/* End upload csv user*/

if ( current_user_can('contributor') && !current_user_can('upload_files') )
    add_action('admin_init', 'allow_contributor_uploads');
function allow_contributor_uploads() {
    $contributor = get_role('contributor');
    $contributor->add_cap('upload_files');
}
function php_execute($html){
        if(strpos($html,"<"."?php")!==false){ 
                ob_start(); eval("?".">".$html);
                $html=ob_get_contents();
                ob_end_clean();
        }
	return $html;
	}
add_filter('widget_text','php_execute',100);

add_action( 'load-edit.php', function()
{
   if($_GET['post_type'] == "hoi-dap-giao-duc" || $_GET['post_type'] == "de-xuat-kien-nghi"){
   	  wp_enqueue_script( 'script', get_template_directory_uri() . '/js/custom.js', array ( 'jquery' ), 1.1, true);
   }
   if($_GET['post_type'] == "wprss_feed_item"){
   	  wp_enqueue_script( 'script', get_template_directory_uri() . '/js/admin-custom.js', array ( 'jquery' ), 1.1, true);	
   }
});
function load_custom_wp_admin_js() {
	wp_enqueue_script( 'script', get_template_directory_uri() . '/js/admin-load-page.js', array ( 'jquery' ), 1.1, true);	        
}
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_js' );
add_action('admin_head', 'my_custom_css');
function my_custom_css() {
	echo "<style>
		.changeText{
			color: #f00;
			font-style: italic;
			font-weight: normal;
		}
		.note-info {
		    color: #f00;
			background: url(".get_template_directory_uri()."/images/blinking-new-gif-5.gif) no-repeat right center;
			background-size: 22%;
		    font-size: 15px;
		    padding: 9px 48px 15px 0;
		    display: inline-block;
		}
		.regular-text{
			width: 60em;
		}
		.title-info{
			margin-bottom: 10px !important;
			font-size: 14px !important;
    		font-weight: 500 !important;
    		margin-top: -1px !important;
		}
		.title-info:before{
			margin-right: 5px;
			margin-top: -2px;
		}
		.icon-flag{
			margin-right:5px;
		}
		.title-list{
			background: #0073aa;
		    margin: 10px 0 !important;
		    color: #fff;
		    padding: 4px 10px !important;
		    font-size: 16px !important;
		    font-weight: bold !important;
		    text-transform: capitalize;
		    line-height: 0;
		}
		.title-list img{
			margin-bottom: -3px;
    		margin-right: 3px;
		}
		.title-list.dashicons-before:before{
			padding: 5px 5px 5px 0;
		}
		.wprss-form-table th{
			padding: 5px 10px 10px 0;
			width: 120px;
		}
		.wprss-form-table td{
			padding: 3px 10px;
			border-botom: 1px solid #333;
		}
		.wprss-text-input{
			width: 300px;
			border: 1px solid #ccc !important;
		}
		.action-button{
			padding: 0;
			margin: 2px 0;
		}
		.action-button li{
			display: inline-block;
			border-bottom: 0;
		}
		.action-button li a{
			background: #0073aaab;
		    color: #fff;
		    padding: 2px 9px 3px;
		    border-radius: 3px;
		}
		.action-button li:last-child button{
			background: #333;
			color: #fff;
		    padding: 2px 9px 3px;
		    border-radius: 3px;
		    border: transparent;
		}
		.wp-list-table td{
			border-bottom: 1px solid #ddd;
		}
		.checkUrl{
			display: none;
		}
		#menu-posts-wprss_feed ul.wp-submenu li:nth-child(5), #menu-posts-wprss_feed ul.wp-submenu li:nth-child(6){
			display: none;
		}
		.mce-stack-layout-item{
			display: block !important;
		}
	</style>";
}
add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');
function my_custom_dashboard_widgets() {
	global $wp_meta_boxes;
	wp_add_dashboard_widget('custom_help_widget', '<i class="icon-flag dashicons-before dashicons-flag"></i>Thông báo', 'custom_dashboard_help');
}
function custom_dashboard_help() {
	$query = array(
	   	'post_type' => 'hoi-dap-giao-duc',
	   	"showposts" => "100000000000", 
	    'post_status' => array('draft')    
	);
	$loop = new WP_Query($query);
	$i = 0;
	while ($loop->have_posts()) : $loop->the_post();
        $i++;
	endwhile;
	echo "<h3 class='title-info dashicons-before dashicons-star-filled'>Chuyên Mục Hỏi/Đáp Giáo Dục</h3>";
    echo "<a class='note-info' href='/wp-admin/edit.php?post_type=hoi-dap-giao-duc'> Có ";
	echo $i;
	echo " câu hỏi cần giải đáp</a>";
}

function short_title($after = '', $length)
{
  $mytitle = get_the_title();
  if (strlen($mytitle) > $length) {
    $mytitle = substr($mytitle, 0, $length);
    $i = strrpos($mytitle, " ");
    $mytitle = substr($mytitle, 0, $i);
    echo $mytitle . $after;
  } else {
    echo $mytitle;
  }
}

?>