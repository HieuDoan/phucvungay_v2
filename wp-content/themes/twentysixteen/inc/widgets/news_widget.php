<?php
/********************************************************************
// Widget news
********************************************************************/
class news_widget extends WP_Widget {

  # Constructor
  function news_widget() {
    $widget_ops = array('description' => 'Tin tức');
    $this->WP_Widget('news_widget', '+ Tin Tức', $widget_ops);
  }

  # Display Widget
  function widget($args, $instance) {
    extract($args);
    
    # Instances
    $showpost_number = $instance['showpost_number'];
    $showpost_title = $instance['showpost_title'];
    $showpost_des = $instance['showpost_des'];
    $categories = $instance['categories'];
    
    # Display
    ?>
    <?php
      $args = array ( 
          'cat' => $categories,
          'post_status' => 'publish',
          'post_type' => "post",
          'showposts' => $showpost_number,
        );
    ?>
    <div class="widget news" id="news">
      <div class="w-content news-content">
        <h2 class="title-news"><?php echo $showpost_title; ?></h2>
        <p class="dess"><?php echo $showpost_des; ?></p>
        <ul class="row list-news">
            <?php 
              $getposts = new WP_query($args); 
              $i = 0;
              $j = 0;
              if ($getposts->have_posts()) : 
                  while ($getposts->have_posts()) : $getposts->the_post(); 
                    if($i == 0): ?>
                      <li class="col-xl-6 col-lg-6 col-md-12 col-sm-6 col-12 first">
                        <?php ddev_thumb(500,400); ?>
                        <div class="title_main">
                          <p class="date">
                            <?php echo get_the_time('M d, Y'); ?>
                          </p>
                          <h4>
                            <?php the_title(); ?>
                          </h4>  
                         <p class="text-center"><a href="<?php the_permalink(); ?>" class="morethan">Đọc thêm</a></p> 
                        </div>
                      </li>
                    <?php endif; ?>
                  <?php $i++; endwhile; ?> 
                  <li class="col-xl-6 col-lg-6 col-md-12 col-sm-6 col-12 second">
                    <ul class="row">
                      <?php while ($getposts->have_posts()) : $getposts->the_post(); ?>
                        <?php if($j> 0): ?>
                          <?php if($j == 1): ?>
                            <li class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 second">
                              <?php ddev_thumb(500,200); ?>
                              <div class="title_main">
                                <p class="date">
                                  <?php echo get_the_time('M d, Y'); ?>
                                </p>
                                <h4>
                                  <?php the_title(); ?>
                                </h4>  
                               <p class="text-center"><a href="<?php the_permalink(); ?>" class="morethan">Đọc thêm</a></p> 
                              </div>
                            </li> 
                          <?php else: ?>
                            <li class="col-xl-6 col-lg-6 col-md-12 col-sm-6 col-12">
                              <?php ddev_thumb(500,400); ?>
                              <div class="title_main">
                                <p class="date">
                                  <?php echo get_the_time('M d, Y'); ?>
                                </p>
                                <h4>
                                  <?php the_title(); ?>
                                </h4>  
                               <p class="text-center"><a href="<?php the_permalink(); ?>" class="morethan">Đọc thêm</a></p> 
                              </div>
                            </li>
                          <?php endif; ?>
                        <?php endif; ?>
                    <?php $j++; endwhile; ?>
                    </ul>
                  </li>
                <?php endif; ?>
            </ul>
        </div>
    </div><!--End .news-->
    <?php
  }

  # Update form
  function update($new_instance, $old_instance) {
    if (!isset($new_instance['submit'])) {
      return false;
    }
    # Instances (old = new)
    $instance = $old_instance;
    $instance['showpost_number'] = $new_instance['showpost_number'];
    $instance['showpost_title'] = $new_instance['showpost_title'];
    $instance['showpost_des'] = $new_instance['showpost_des'];
    $instance['categories'] = $new_instance['categories'];
    return $instance;
  }

  # Options form
  function form($instance) {
    global $wpdb;
    # Instances
    $instance = wp_parse_args((array) $instance, array('title' => ''));
    $showpost_number = $instance['showpost_number'];
    $showpost_title = $instance['showpost_title'];
    $showpost_des = $instance['showpost_des'];
    $categories = $instance['categories'];
?>
<p>
  <label for="<?php echo $this->get_field_id('showpost_title'); ?>">Tiêu đề:</label>
  <input class="widefat" id="<?php echo $this->get_field_id('showpost_title'); ?>" name="<?php echo $this->get_field_name('showpost_title'); ?>" type="text" value="<?php echo $showpost_title; ?>" />
</p>

<p>
  <label for="<?php echo $this->get_field_id('showpost_des'); ?>">Mô tả:</label>
  <textarea class="widefat" id="<?php echo $this->get_field_id('showpost_des'); ?>" name="<?php echo $this->get_field_name('showpost_des'); ?>" type="text"><?php echo $showpost_des; ?></textarea>
</p>

<p>
      <label for="<?php echo $this->get_field_id('categories'); ?>"><?php _e('Category Product:', 'uxde') ?></label> 
      <?php 
        $categories = get_categories('hide_empty=0&depth=1&type=post');
      ?>
      <select id="<?php echo $this->get_field_id('categories'); ?>" name="<?php echo $this->get_field_name('categories'); ?>" class="widefat categories">
        <option value='all' <?php if ('all' == $instance['categories']) echo 'selected="selected"'; ?>>All categories</option>
        <?php foreach($categories as $category) { ?>
          <option value='<?php echo $category->term_id; ?>' <?php if ($category->term_id == $instance['categories']) echo 'selected="selected"'; ?>><?php echo $category->name; ?></option>       
        <?php }?>
      </select>
    </p>
<p>
  <label for="<?php echo $this->get_field_id('showpost_number'); ?>">Số bài viết:</label>
  <input class="widefat" id="<?php echo $this->get_field_id('showpost_number'); ?>" name="<?php echo $this->get_field_name('showpost_number'); ?>" type="text" value="<?php echo $showpost_number; ?>" />
</p>

<input type="hidden" id="<?php echo $this->get_field_id('submit'); ?>" name="<?php echo $this->get_field_name('submit'); ?>" value="1" />

<?php
  }
}

### Initiate widget
add_action('widgets_init', 'news_widget_box');
function news_widget_box() {
  register_widget('news_widget');
}




?>