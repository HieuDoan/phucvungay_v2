<?php
/********************************************************************
// 20171105 - HieuDT code upload image
********************************************************************/
add_action('widgets_init', 'upload_image_process_widget');
function upload_image_process_widget() {
    register_widget( 'process' );
}

add_action('admin_enqueue_scripts', 'upload_image_process_wdscript');
function upload_image_process_wdscript() {
    wp_enqueue_media();
    wp_enqueue_script('intro_script', get_template_directory_uri() . '/js/uploadImage.js', false, '1.0', true);
}

class process extends WP_Widget {
    function process() {
        $widget_ops = array('classname' => 'col-lg-6 col-xl-6 col-lg-6 col-sm-6 col-12 text-center d-none d-sm-block');
        $this->WP_Widget('ctUp-process-widget', '+ Lộ trình phát triển', $widget_ops);
    }
    function widget($args, $instance) {
        extract($args);
        echo $before_widget;
    ?>
    <ul class="list-process">
        <li class="active">
            <?php echo $instance['process1']; ?>
        </li>
        <li>
            <?php echo $instance['process2']; ?>
        </li>
        <li class="third">
             <?php echo $instance['process3']; ?>   
        </li>
        <li>
            <?php echo $instance['process4']; ?>
        </li>
    </ul>
    <?php
      echo $after_widget;
  }
    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['process1'] = strip_tags( $new_instance['process1'] );
        $instance['process2'] = strip_tags( $new_instance['process2'] );
        $instance['process3'] = strip_tags( $new_instance['process3'] );
        $instance['process4'] = strip_tags( $new_instance['process4'] );
        return $instance;
    }
    function form($instance) {
  ?>
    <p>
        <label>Lộ trình 1: </label>
        <input type="text" class="widefat custom_media_url" name="<?php echo $this->get_field_name('process1'); ?>" id="<?php echo $this->get_field_id('process1'); ?>" value="<?php echo $instance['process1']; ?>" style="margin-top:5px;">
    </p>
    <p>
        <label>Lộ trình 2: </label>
        <input type="text" class="widefat custom_media_url" name="<?php echo $this->get_field_name('process2'); ?>" id="<?php echo $this->get_field_id('process2'); ?>" value="<?php echo $instance['process2']; ?>" style="margin-top:5px;">
    </p>
    <p>
        <label>Lộ trình 3: </label>
        <input type="text" class="widefat custom_media_url" name="<?php echo $this->get_field_name('process3'); ?>" id="<?php echo $this->get_field_id('process3'); ?>" value="<?php echo $instance['process3']; ?>" style="margin-top:5px;">
    </p>
    <p>
        <label>Lộ trình 4: </label>
        <input type="text" class="widefat custom_media_url" name="<?php echo $this->get_field_name('process4'); ?>" id="<?php echo $this->get_field_id('process4'); ?>" value="<?php echo $instance['process4']; ?>" style="margin-top:5px;">
    </p>
<?php
    }
}

/********************************************************************
// End HieuDT
********************************************************************/
?>