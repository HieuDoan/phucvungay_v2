<?php
/********************************************************************
// Widget recruit
********************************************************************/
add_action('admin_enqueue_scripts', 'upload_image_as_p_wdscript');
function upload_image_as_p_wdscript() {
    wp_enqueue_media();
    wp_enqueue_script('as_script', get_template_directory_uri() . '/js/uploadImage.js', false, '1.0', true);
}
class recruit_widget extends WP_Widget {

  # Constructor
  function recruit_widget() {
    $widget_ops = array('description' => 'recruit');
    $this->WP_Widget('recruit_widget', '+ recruit', $widget_ops);
  }

  # Display Widget
  function widget($args, $instance) {
    extract($args);
    
    # Instances
    $showpost_number = $instance['showpost_number'];
    $showpost_title = $instance['showpost_title'];
    $showpost_des = $instance['showpost_des'];
    # Display
    ?>
    
    <div class="recruit row">
      <!-- <div class="w-content recruit-content"> -->
        <div class="col-lg-12 col-xl-12 col-lg-12 col-sm-12 col-12">
           <div class="mar-recruit">
              <h2 class="title-recruit"><?php echo $showpost_title; ?></h2>
              <?php echo $showpost_des; ?>
            </div> 
        </div>
        <div class="col-lg-12 col-xl-12 col-lg-7 col-sm-12 col-12">
            <a href="#" class="image-rs">
              <img src="<?php echo esc_url($instance['image_uri']); ?>" class="img-responsive"/>
          </a>
        </div>  
        
    </div>
    <?php
  }

  # Update form
  function update($new_instance, $old_instance) {
    if (!isset($new_instance['submit'])) {
      return false;
    }
    # Instances (old = new)
    $instance = $old_instance;
    //$instance['showpost_number'] = $new_instance['showpost_number'];
    $instance['showpost_title'] = $new_instance['showpost_title'];
    $instance['showpost_des'] = $new_instance['showpost_des'];
    $instance['image_uri'] = strip_tags( $new_instance['image_uri'] );

    return $instance;
  }

  # Options form
  function form($instance) {
    global $wpdb;
    # Instances
    $instance = wp_parse_args((array) $instance, array('title' => ''));
    //$showpost_number = $instance['showpost_number'];
    $showpost_title = $instance['showpost_title'];
    $showpost_des = $instance['showpost_des'];
?>
<p>
  <label for="<?php echo $this->get_field_id('showpost_title'); ?>">Tiêu đề:</label>
  <input class="widefat" id="<?php echo $this->get_field_id('showpost_title'); ?>" name="<?php echo $this->get_field_name('showpost_title'); ?>" type="text" value="<?php echo $showpost_title; ?>" />
</p>
<p>
  <label for="<?php echo $this->get_field_id('showpost_des'); ?>">Mô tả:</label>
  <textarea rows="5" class="widefat" id="<?php echo $this->get_field_id('showpost_des'); ?>" name="<?php echo $this->get_field_name('showpost_des'); ?>"><?php echo $showpost_des; ?></textarea>
</p>
<p>
    <label for="<?php echo $this->get_field_id('image_uri'); ?>">Image</label><br />
    <input type="text" class="widefat custom_media_url" name="<?php echo $this->get_field_name('image_uri'); ?>" id="<?php echo $this->get_field_id('image_uri'); ?>" value="<?php echo $instance['image_uri']; ?>" style="margin-top:5px;">

    <input type="button" class="button button-primary custom_media_button" id="custom_media_button" name="<?php echo $this->get_field_name('image_uri'); ?>" value="Upload Image" style="margin-top:5px;" />
</p>
<!-- <p>
  <label for="<?php echo $this->get_field_id('showpost_number'); ?>">Số bài viết:</label>
  <input class="widefat" id="<?php echo $this->get_field_id('showpost_number'); ?>" name="<?php echo $this->get_field_name('showpost_number'); ?>" type="text" value="<?php echo $showpost_number; ?>" />
</p> -->

<input type="hidden" id="<?php echo $this->get_field_id('submit'); ?>" name="<?php echo $this->get_field_name('submit'); ?>" value="1" />

<?php
  }
}

### Initiate widget
add_action('widgets_init', 'recruit_widget_box');
function recruit_widget_box() {
  register_widget('recruit_widget');
}




?>