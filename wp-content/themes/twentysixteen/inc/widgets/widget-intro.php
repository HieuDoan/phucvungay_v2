<?php
/********************************************************************
// 20171105 - HieuDT code upload image
********************************************************************/
add_action('widgets_init', 'upload_image_widget');
function upload_image_widget() {
    register_widget( 'intro' );
}

add_action('admin_enqueue_scripts', 'upload_image_wdscript');
function upload_image_wdscript() {
    wp_enqueue_media();
    wp_enqueue_script('intro_script', get_template_directory_uri() . '/js/uploadImage.js', false, '1.0', true);
}

class intro extends WP_Widget {
    function intro() {
        $widget_ops = array('classname' => 'col-xl-12 col-lg-12 col-sm-12 col-12 infomation_block');
        $this->WP_Widget('ctUp-intro-widget', '+ Giới Thiệu Về Công Ty', $widget_ops);
    }
    function widget($args, $instance) {
        extract($args);
        echo $before_widget;
    ?>
    <div class="row">
      <div class="col-xl-6 col-lg-6 col-sm-6 col-md-6 col-12">
        <h3>
          <?php echo $instance['name_info']; ?>
        </h3>
        <p class="txt-left-info">
            <?php echo $instance['content'];  ?>
        </p>
        <a href="/ve-chung-toi" class="text-left more-info">Tìm hiểu thêm</a>
      </div>
      <div class="col-xl-6 col-lg-6 col-sm-6 col-md-6 col-12">
        <a href="#" class="image-rs">
          <img src="<?php echo esc_url($instance['image_uri']); ?>" class="img-responsive"/>
        </a>
      </div>
    </div>
      
    <?php
      echo $after_widget;
  }
    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['text'] = strip_tags( $new_instance['text'] );
        $instance['image_uri'] = strip_tags( $new_instance['image_uri'] );
        $instance['name_info'] = strip_tags( $new_instance['name_info'] );
        $instance['content'] = strip_tags( $new_instance['content'] );
        return $instance;
    }
    function form($instance) {
  ?>
    <p>
        <label">Tiêu đề: </label><br />
        <input type="text" class="widefat" name="<?php echo $this->get_field_name('name_info'); ?>" id="<?php echo $this->get_field_id('name_info'); ?>" value="<?php echo $instance['name_info']; ?>" style="margin-top:5px;">
    </p>
    <p>
        <label">Nội dung: </label><br />
        <textarea name="<?php echo $this->get_field_name('content'); ?>" id="<?php echo $this->get_field_id('content'); ?>" style="width: 100%; resize: none; "><?php echo $instance['content']; ?>
        </textarea> 
    </p>
    <p>
        <label for="<?php echo $this->get_field_id('image_uri'); ?>">Image</label><br />
        <input type="text" class="widefat custom_media_url" name="<?php echo $this->get_field_name('image_uri'); ?>" id="<?php echo $this->get_field_id('image_uri'); ?>" value="<?php echo $instance['image_uri']; ?>" style="margin-top:5px;">

        <input type="button" class="button button-primary custom_media_button" id="custom_media_button" name="<?php echo $this->get_field_name('image_uri'); ?>" value="Upload Image" style="margin-top:5px;" />
    </p>
<?php
    }
}

/********************************************************************
// End HieuDT
********************************************************************/
?>