<?php
/********************************************************************
// Widget contact
********************************************************************/
class contact_widget extends WP_Widget {

  # Constructor
  function contact_widget() {
    $widget_ops = array('description' => 'contact');
    $this->WP_Widget('contact_widget', '+ contact', $widget_ops);
  }

  # Display Widget
  function widget($args, $instance) {
    extract($args);
    
    # Instances
    $showpost_des = $instance['showpost_des'];
    $showpost_address = $instance['showpost_address'];
    $showpost_mail = $instance['showpost_mail'];
    $showpost_phone = $instance['showpost_phone'];
    $showpost_fax = $instance['showpost_fax'];
    $showpost_web = $instance['showpost_web'];
    # Display
    ?>
    <div class="top-contact">
        <div class="vc_column-inner">
            <h2 class="title-contact">Liên hệ</h2>
            <p><?php echo $showpost_des; ?></p>
        </div>
        <div class="container">
          <div class="w-content contact-content">
              <ul class="row">
                  <li class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-4">
                      <div>
                          <img src="<?php echo get_bloginfo('template_directory');?>/img/contact-icon1.png">
                            <h4>văn phòng</h4>
                          <p>
                            <?php echo $showpost_address; ?>
                          </p>
                          <div class="line"></div>
                      </div>
                  </li>
                  <li class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-4">
                      <div>
                          <img src="<?php echo get_bloginfo('template_directory');?>/img/contact-icon2.png">
                          <h4>Thư điện tử</h4>
                          <p>
                            Email: <?php echo $showpost_mail; ?><br>
                            Website: <?php echo $showpost_web; ?>
                          </p>
                          <div class="line"></div>
                      </div>
                  </li>
                  <li class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-4">
                      <div>
                          <img src="<?php echo get_bloginfo('template_directory');?>/img/contact-icon3.png">
                          <h4>Support</h4>
                          <p>
                            Phone: <?php echo $showpost_phone; ?><br>
                            Fax: <?php echo $showpost_fax; ?>
                          </p>
                          <div class="line"></div>
                      </div>
                  </li>
              </ul>
            </div>
        </div>
    </div><!--End .contact-->
    
    <?php
  }

  # Update form
  function update($new_instance, $old_instance) {
    if (!isset($new_instance['submit'])) {
      return false;
    }
    # Instances (old = new)
    $instance = $old_instance;
    $instance['showpost_des'] = $new_instance['showpost_des'];
    $instance['showpost_address'] = $new_instance['showpost_address'];
    $instance['showpost_mail'] = $new_instance['showpost_mail'];
    $instance['showpost_phone'] = $new_instance['showpost_phone'];
    $instance['showpost_fax'] = $new_instance['showpost_fax'];
    $instance['showpost_web'] = $new_instance['showpost_web'];
    return $instance;
  }

  # Options form
  function form($instance) {
    global $wpdb;
    # Instances
    $instance = wp_parse_args((array) $instance, array('title' => ''));
    $showpost_des = $instance['showpost_des'];
    $showpost_address = $instance['showpost_address'];
    $showpost_mail = $instance['showpost_mail'];
    $showpost_phone = $instance['showpost_phone'];
    $showpost_fax = $instance['showpost_fax'];
    $showpost_web = $instance['showpost_web'];
?>
<p>
  <label for="<?php echo $this->get_field_id('showpost_des'); ?>">Mô tả:</label>
  <textarea rows="5" class="widefat" id="<?php echo $this->get_field_id('showpost_des'); ?>" name="<?php echo $this->get_field_name('showpost_des'); ?>"><?php echo $showpost_des; ?></textarea>
</p>
<h3>Văn Phòng</h3>
<p>
  <label for="<?php echo $this->get_field_id('showpost_address'); ?>">Địa chỉ:</label>
  <input class="widefat" id="<?php echo $this->get_field_id('showpost_address'); ?>" name="<?php echo $this->get_field_name('showpost_address'); ?>" value="<?php echo $showpost_address; ?>">
</p>
<h3>Thư Điện Tử</h3>
<p>
  <label for="<?php echo $this->get_field_id('showpost_mail'); ?>">Mail:</label>
  <input type="text" class="widefat" id="<?php echo $this->get_field_id('showpost_mail'); ?>" name="<?php echo $this->get_field_name('showpost_mail'); ?>" value="<?php echo $showpost_mail; ?>">
</p>
<p>
  <label for="<?php echo $this->get_field_id('showpost_web'); ?>">Website:</label>
  <input type="text" class="widefat" id="<?php echo $this->get_field_id('showpost_web'); ?>" name="<?php echo $this->get_field_name('showpost_web'); ?>" value="<?php echo $showpost_web; ?>">
</p>
<h3>Support</h3>
<p>
  <label for="<?php echo $this->get_field_id('showpost_phone'); ?>">Phone:</label>
  <input type="text" class="widefat" id="<?php echo $this->get_field_id('showpost_phone'); ?>" name="<?php echo $this->get_field_name('showpost_phone'); ?>" value="<?php echo $showpost_phone; ?>">
</p>
<p>
  <label for="<?php echo $this->get_field_id('showpost_fax'); ?>">Fax:</label>
  <input type="text" class="widefat" id="<?php echo $this->get_field_id('showpost_fax'); ?>" name="<?php echo $this->get_field_name('showpost_fax'); ?>" value="<?php echo $showpost_fax; ?>">
</p>

<input type="hidden" id="<?php echo $this->get_field_id('submit'); ?>" name="<?php echo $this->get_field_name('submit'); ?>" value="1" />

<?php
  }
}

### Initiate widget
add_action('widgets_init', 'contact_widget_box');
function contact_widget_box() {
  register_widget('contact_widget');
}




?>