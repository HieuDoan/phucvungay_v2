<?php
/********************************************************************
// Widget product
********************************************************************/
class product_widget extends WP_Widget {

  # Constructor
  function product_widget() {
    $widget_ops = array('description' => 'product');
    $this->WP_Widget('product_widget', '+ product', $widget_ops);
  }

  # Display Widget
  function widget($args, $instance) {
    extract($args);
    
    # Instances
    $showpost_number = $instance['showpost_number'];
    $showpost_title = $instance['showpost_title'];
    
    # Display
    ?>
    
    <div class="widget product" id="product">
      <div class="w-content product-content">
        <h2 class="title-product"><?php echo $showpost_title; ?></h2>
        <ul>
          <?php
            $args = array ( 'post_status' => 'publish',
                            'post_type' => "product",
                            'showposts' => $showpost_number,
                          );
          ?>
          <?php $getposts = new WP_query($args); ?>
          <?php if ($getposts->have_posts()) : ?>
            <?php while ($getposts->have_posts()) : $getposts->the_post(); ?>
              <li class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">

                <div class="title_main">
                  <img src="<?php echo types_render_field('icon', array('output' => 'raw')); ?>">
                  <h4>
                    <?php the_title();?>
                  </h4>  
                </div>

                  <?php the_post_thumbnail( 'product-thumb' ); ?>
                  
              </li>
            <?php endwhile; ?>
          <?php endif; ?>
        </ul>
      </div>
    </div><!--End .product-->
    
    <?php
  }

  # Update form
  function update($new_instance, $old_instance) {
    if (!isset($new_instance['submit'])) {
      return false;
    }
    # Instances (old = new)
    $instance = $old_instance;
    $instance['showpost_number'] = $new_instance['showpost_number'];
    $instance['showpost_title'] = $new_instance['showpost_title'];
    return $instance;
  }

  # Options form
  function form($instance) {
    global $wpdb;
    # Instances
    $instance = wp_parse_args((array) $instance, array('title' => ''));
    $showpost_number = $instance['showpost_number'];
?>
<p>
  <label for="<?php echo $this->get_field_id('showpost_title'); ?>">Tiêu đề:</label>
  <input class="widefat" id="<?php echo $this->get_field_id('showpost_title'); ?>" name="<?php echo $this->get_field_name('showpost_title'); ?>" type="text" value="<?php echo $showpost_title; ?>" />
</p>
<p>
  <label for="<?php echo $this->get_field_id('showpost_number'); ?>">Số bài viết:</label>
  <input class="widefat" id="<?php echo $this->get_field_id('showpost_number'); ?>" name="<?php echo $this->get_field_name('showpost_number'); ?>" type="text" value="<?php echo $showpost_number; ?>" />
</p>

<input type="hidden" id="<?php echo $this->get_field_id('submit'); ?>" name="<?php echo $this->get_field_name('submit'); ?>" value="1" />

<?php
  }
}

### Initiate widget
add_action('widgets_init', 'product_widget_box');
function product_widget_box() {
  register_widget('product_widget');
}




?>