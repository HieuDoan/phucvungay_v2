<?php
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php endif; ?>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<div id="page" class="">
		<header id="masthead" class="site-header" role="banner">
			<div class="info-top-header d-xl-none d-sm-none">
				<div class="info-header">
					<ul>
						<li>
							Hỗ trợ trực tuyến
						</li>
						<li>
							0937526333
						</li>
					</ul>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-3">
						<?php twentysixteen_the_custom_logo(); ?>
					</div>
					<div class="col-9 d-lg-none d-xl-none d-md-none">
						<button id="menu-toggle" class="menu-toggle col-xs-3 float-right"><i class="fas fa-bars"></i></button>
					</div>
					<div id="site-header-menu" class="site-header-menu col-12 col-lg-10 col-md-10 col-sm-10">
						<div class="info-top-header d-none d-sm-block">
							<div class="info-header">
								<ul>
									<li>
										Hỗ trợ trực tuyến
									</li>
									<li>
										0937526333
									</li>
								</ul>
							</div>
						</div>
						<?php if ( has_nav_menu( 'primary' ) ) : ?>
							<nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Primary Menu', 'twentysixteen' ); ?>">
								<?php
								wp_nav_menu( array(
									'theme_location' => 'primary',
									'menu_class'     => 'primary-menu',
								) );
								?>
							</nav>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</header>
		<div id="content" class="">
