<?php get_header(); ?>
<div id="main-body">
	<div class="container">
		<div class="main-body">
			<div class="row">
				<div class="col-xs-12 col-md-12 col-lg-12 col-xl-12 content">
					<div id="main-content" class="main-cate-new">
							<div class="row">
								<div class="col-lg-6 col-md-6 col-xl-6 col-sm-6 col-xs-12 col-12">
									<?php echo do_shortcode('[contact-form-7 id="208" title="Contact"]')?>
								</div>
								<div class="col-lg-6 col-md-6 col-xl-6 col-sm-6 col-xs-12 col-12">
									<?php dynamic_sidebar('contact'); ?>
								</div>
							</div><!--End .s-content-->
					</div><!--End .main-content-->
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>