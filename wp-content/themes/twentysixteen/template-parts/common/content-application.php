<article class="application">
	<div class="panel-content">
		<div class="container">
			<div class="row">
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-4 col-12">
					<p class="title">Tải ứng dụng fserving  </p>
					<div class="main-app">
						<div class="row">
							<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
								<p>Tải ứng dụng tại</p>
								<ul class="list-app">
									<li class="list-app-item"><img src="<?php echo get_bloginfo('template_directory');?>/img/app_store.png"></li>
									<li class="list-app-item"><img src="<?php echo get_bloginfo('template_directory');?>/img/google_play.png"></li>
								</ul>
							</div>
							<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-12 center">
								<p>
									Hoặc
								</p>
							</div>
							<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
								<p>Quét mã QR</p>
								<img src="<?php echo get_bloginfo('template_directory');?>/img/qr_code.png">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
