<article class="product" id="forme">
	<div class="panel-content">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<p class="title">Phục vụ nhanh</p>
					<p class="sub-title">fserving</p>
				</div>

				<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12 product-info">

					<div class="title_main">
						<img src="<?php bloginfo('template_url'); ?>/img/car.png">
						<h4>
						Được phục vụ ngay lập tức                  </h4>
					</div>


				</div>
				<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12 product-info ">

					<div class="title_main">
						<img src="<?php bloginfo('template_url'); ?>/img/payment.png">
						<h4>
						Thanh toán đơn giản và rút ngắn thời gian                  </h4>
					</div>


				</div>
				<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12 product-info">

					<div class="title_main">
						<img src="<?php bloginfo('template_url'); ?>/img/info_icon.png">
						<h4>
						Thông tin chính xác rõ ràng                  </h4>
					</div>


				</div>
				<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12 product-info">

					<div class="title_main">
						<img src="<?php bloginfo('template_url'); ?>/img/search_icon.png">
						<h4>
						Tìm kiếm đơn giản                  </h4>
					</div>


				</div>
				<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12 product-info">

					<div class="title_main">
						<img src="<?php bloginfo('template_url'); ?>/img/connect_icon.png">
						<h4>
						Kết nối mọi người                  </h4>
					</div>


				</div>
				<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12 product-info">

					<div class="title_main">
						<img src="<?php bloginfo('template_url'); ?>/img/promotion_icon.png">
						<h4>Thêm nhiều ưu đãi mỗi ngày </h4>
					</div>


				</div>
			</div>
		</div>
	</div>
</article>
