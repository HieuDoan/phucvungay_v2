<div id="demo" class="carousel slide" data-ride="carousel">

  <ul class="carousel-indicators">
    <li data-target="#demo" data-slide-to="0" class="active"></li>
    <li data-target="#demo" data-slide-to="1"></li>
    <li data-target="#demo" data-slide-to="2"></li>
  </ul>
  
  <div class="carousel-inner">
    <div class="carousel-item active">
    	<div class="row">
        	<div class="banner-column-left col-lg-7 col-md-7 col-sm-7 col-xs-12 text-left">
				<p class="p0">fserving</p>
				<p class="p1">cùng trải nghiệm </p>
				<p class="p2">dịch vụ và ưu đãi<br>cực kỳ hấp dẫn! </p>
				<a href="#"> Đăng ký </a>
			</div>
			<div class="banner-column-right col-lg-5 col-md-5 col-sm-5 col-xs-12 text-left">
				<img src="<?php echo ddev_get_setting('ddev_banner_url'); ?>" />
			</div>	
        </div>
        
    </div>
    <div class="carousel-item">
      	<div class="row">
        	<div class="banner-column-left col-lg-7 col-md-7 col-sm-7 col-xs-12 text-left">
				<p class="p0">fserving</p>
				<p class="p1">cùng trải nghiệm </p>
				<p class="p2">dịch vụ và ưu đãi<br>cực kỳ hấp dẫn! </p>
				<a href="#"> Đăng ký </a>
			</div>
			<div class="banner-column-right col-lg-5 col-md-5 col-sm-5 col-xs-12 text-left">
				<img src="<?php echo ddev_get_setting('ddev_banner_mobile_url'); ?>" />
			</div>	
        </div>
    </div>
    <div class="carousel-item">
      	<div class="row">
        	<div class="banner-column-left col-lg-7 col-md-7 col-sm-7 col-xs-12 text-left">
				<p class="p0">fserving</p>
				<p class="p1">cùng trải nghiệm </p>
				<p class="p2">dịch vụ và ưu đãi<br>cực kỳ hấp dẫn! </p>
				<a href="#"> Đăng ký </a>
			</div>
			<div class="banner-column-right col-lg-5 col-md-5 col-sm-5 col-xs-12 text-left">
				<img src="<?php echo ddev_get_setting('ddev_banner_url_slider3'); ?>" />
			</div>	
        </div>
    </div>
  </div>
</div>