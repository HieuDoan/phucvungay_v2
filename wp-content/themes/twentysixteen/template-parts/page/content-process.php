<article class="top-feature">
	<div class="panel-content">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-xl-6 col-lg-6 col-sm-12 col-md-12 col-12 text-center d-none d-sm-block">
					<div class="feature">
						<p class="title">Những tính năng nổi bật của Fservings?</p>
						<p class="description">Dễ dàng tìm kiếm những dịch vụ tốt nhất </p>
					</div>
					<ul class="list-process">
						<li class="first">
							<p class="feature-title">Nhanh</p>
							<p class="feature-description">Phục vụ nhanh nhất các nhu cầu thiết yếu<br> trong cuộc sống</p>
						</li>
						<li class="second">
							<p class="feature-title">Tiện lợi</p>
							<p class="feature-description">Chọn những dịch vụ tốt nhất với<br> thời gian nhanh và thao tác đơn giản</p>
						</li>
					</ul>
					<ul class="list-process list-process-none">
						<li class="third">
							<p class="feature-title">Tiết kiệm chi phí</p>
							<p class="feature-description">So sánh giá trị thường và đưa ra mức mua<br> phù hợp nhất đi kèm với nhiều ưu đãi hấp dẫn </p>
						</li>
					</ul>
				</div>
				<div class="col-lg-6 col-xl-6 col-lg-6 col-sm-12 col-md-12 col-12">
					<img src="<?php bloginfo('template_url'); ?>/img/group_icon.png" />
				</div>
			</div>
		</div>
	</div>
</article>
