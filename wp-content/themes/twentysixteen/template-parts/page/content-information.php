<article class="information">
	<div class="panel-content">
		<div class="container">
			<div class="row">
				<div class="col-xl-12 col-lg-12 col-md-12 col-xs-12 col-sm-12">
					<?php dynamic_sidebar("information"); ?>
				</div>
			</div>
		</div><!-- .wrap -->
	</div><!-- .panel-content -->

</article><!-- #post-## -->
