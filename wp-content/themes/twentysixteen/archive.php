
<?php get_header(); ?>
<div id="main-body">
	<div class="container">
		<div class="main-body">
			<div class="line-block"></div>
			<div class="row">
				<div class="col-xs-12 col-md-12 col-lg-12 col-xl-12 content">
					<div id="main-content" class="main-cate-new">
						<div class="single">
							<div class="row">
								<?php if (have_posts()) : ?>
										<?php while (have_posts()) : the_post(); ?>
											<div class="col-lg-4 col-xl-4 col-md-4 col-xs-12 col-12 new-cat">
												<div class="cat-thumb">
													<a href="<?php the_permalink(); ?>"><?php ddev_thumb(500,400); ?></a>
												</div>
												<div class="cat-caption">
													<h2 class="cat-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
													<div class="date cpost-info">
														<i class="far fa-clock"></i>
														<label><?php echo get_the_time('d/m/Y'); ?></label>
													</div>
												</div>
											</div><!--END .cat-item-->
										<?php endwhile; ?>
									<?php if ($wp_query->max_num_pages > 1) ddev_pagenavi(); ?>
								<?php else : ?>
									<div class="alert alert-info">
										<strong>Chưa có dữ liệu !</strong>
									</div>
								<?php endif; ?>
							</div><!--End .s-content-->
						</div><!--End .single-->
					</div><!--End .main-content-->
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>