<?php get_header(); ?>
<div id="main-body">
	<div class="container">
		<div class="main-body">
			<div class="line-block"></div>
			<div class="row">
				<div class="col-xs-12 col-md-12 content">
					<div id="main-content" class="main-content main-content-6">
						<div class="single">
							<?php if (have_posts()) : ?>
							<?php while (have_posts()) : the_post(); global $post;?>
								<div class="s-content" id="post_<?php echo $post->ID; ?>">
									<ul class="breadcrumb-list">
										<li><a href="<?php echo esc_url(home_url()); ?>/">Trang chủ  > </a></li>
										<li><?php the_category(', '); ?>  > </li>
										<li class="active"><?php single_post_title(); ?></li>
									</ul>
									<div class="single-content">
										<h1 class="single-title" id="title-post"><?php the_title(); ?></h1>
										<div class="date cpost-info">
											<p>
												<span><i class="far fa-clock"></i> <label><?php echo get_the_time('d/m/Y'); ?></label></span>
											</p>
										</div>
										<div id="content-post"><?php the_content(); ?></div>
									</div>
									<div class="related">
										<div class="related-title">
											<h3><i class="fa fa-list-ul"></i> BÀI VIẾT CÙNG CHUYÊN MỤC</h3>
										</div>
										<div class="related-content">
											<?php 
												foreach((get_the_category()) as $category) { 
													$cat_id = $category->cat_ID; 
												}

												$args = array ( 'post_status' => 'publish',
																				'category__in' => $cat_id,
																				'post__not_in' => array($post->ID),
																				'showposts' => 6,
																			);
											?>
											<?php $related_post = new WP_query($args); ?>
											<?php if ( $related_post->have_posts()) : ?>
											<ul class="list-group">
												<?php while ( $related_post->have_posts() ): ?>
													<?php $related_post->the_post(); ?>
													<li><i class="fa fa-caret-right"></i> 
														<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
														<span class="time-date-cus">(<?php echo get_the_time('d/m/Y'); ?>)</span>
													</li>
												<?php endwhile; ?>
											</ul>
											<?php endif; ?>
										</div>
									</div><!--End .related-->
								</div><!--End .s-content-->
							<?php endwhile; ?>
							<?php endif; ?>
						</div><!--End .single-->
					</div><!--End .main-content-->
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>
