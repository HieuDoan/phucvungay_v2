<?php
require_once ( get_template_directory() . '/thumbnail.php');
require_once ( get_template_directory() . '/pagenavi.php');
require_once ( get_template_directory() . '/inc/widgets/widget-intro.php');
require_once ( get_template_directory() . '/inc/widgets/widget-process.php');
require_once ( get_template_directory() . '/inc/widgets/product_widget.php');
require_once ( get_template_directory() . '/inc/widgets/news_widget.php');
require_once ( get_template_directory() . '/inc/widgets/news_widget.php');
require_once ( get_template_directory() . '/inc/widgets/team_widget.php');
require_once ( get_template_directory() . '/inc/widgets/contact_widget.php');
require_once ( get_template_directory() . '/inc/widgets/partner_widget.php');
require_once ( get_template_directory() . '/inc/widgets/widget-recruit.php');

if ( version_compare( $GLOBALS['wp_version'], '4.4-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}

if ( ! function_exists( 'twentysixteen_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * Create your own twentysixteen_setup() function to override in a child theme.
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/twentysixteen
	 * If you're building a theme based on Twenty Sixteen, use a find and replace
	 * to change 'twentysixteen' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'twentysixteen' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for custom logo.
	 *
	 *  @since Twenty Sixteen 1.2
	 */
	add_theme_support( 'custom-logo', array(
		'height'      => 240,
		'width'       => 240,
		'flex-height' => true,
	) );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 1200, 9999 );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'twentysixteen' ),
		'social'  => __( 'Social Links Menu', 'twentysixteen' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'status',
		'audio',
		'chat',
	) );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */
	add_editor_style( array( 'css/editor-style.css', twentysixteen_fonts_url() ) );

	// Indicate widget sidebars can use selective refresh in the Customizer.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif; // twentysixteen_setup
add_action( 'after_setup_theme', 'twentysixteen_setup' );

/**
 * Sets the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'twentysixteen_content_width', 840 );
}
add_action( 'after_setup_theme', 'twentysixteen_content_width', 0 );

/**
 * Registers a widget area.
 *
 * @link https://developer.wordpress.org/reference/functions/register_sidebar/
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_widgets_init() {

	register_sidebar( array(
		'name'          => __( 'Về chúng tôi', 'twentysixteen' ),
		'id'            => 'information',
		'description'   => __( 'Appears at the bottom of the content on posts and pages.', 'twentysixteen' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Liên hệ', 'twentysixteen' ),
		'id'            => 'contact',
		'description'   => __( 'Appears at the bottom of the content on posts and pages.', 'twentysixteen' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Tuyển dụng', 'twentysixteen' ),
		'id'            => 'recruit',
		'description'   => __( 'Appears at the bottom of the content on posts and pages.', 'twentysixteen' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => __( 'Tin Tức', 'twentysixteen' ),
		'id'            => 'news',
		'description'   => __( 'Appears at the bottom of the content on posts and pages.', 'twentysixteen' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'twentysixteen_widgets_init' );

if ( ! function_exists( 'twentysixteen_fonts_url' ) ) :
/**
 * Register Google fonts for Twenty Sixteen.
 *
 * Create your own twentysixteen_fonts_url() function to override in a child theme.
 *
 * @since Twenty Sixteen 1.0
 *
 * @return string Google fonts URL for the theme.
 */
function twentysixteen_fonts_url() {
	$fonts_url = '';
	$fonts     = array();
	$subsets   = 'latin,latin-ext';

	/* translators: If there are characters in your language that are not supported by Merriweather, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Merriweather font: on or off', 'twentysixteen' ) ) {
		$fonts[] = 'Merriweather:400,700,900,400italic,700italic,900italic';
	}

	/* translators: If there are characters in your language that are not supported by Montserrat, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Montserrat font: on or off', 'twentysixteen' ) ) {
		$fonts[] = 'Montserrat:400,700';
	}

	/* translators: If there are characters in your language that are not supported by Inconsolata, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Inconsolata font: on or off', 'twentysixteen' ) ) {
		$fonts[] = 'Inconsolata:400';
	}

	if ( $fonts ) {
		$fonts_url = add_query_arg( array(
			'family' => urlencode( implode( '|', $fonts ) ),
			'subset' => urlencode( $subsets ),
		), 'https://fonts.googleapis.com/css' );
	}

	return $fonts_url;
}
endif;

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'twentysixteen_javascript_detection', 0 );

/**
 * Enqueues scripts and styles.
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'twentysixteen-fonts', twentysixteen_fonts_url(), array(), null );

	// Add Genericons, used in the main stylesheet.
	wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.4.1' );

	// Theme stylesheet.
	wp_enqueue_style( 'twentysixteen-common', get_template_directory_uri() . '/css/common.css', array( 'twentysixteen-style' ), '20160816' );
	wp_enqueue_style( 'twentysixteen-style', get_stylesheet_uri() );
	wp_enqueue_style( 'twentysixteen-bootstrap.min', get_template_directory_uri() . '/bootstrap/css/bootstrap.min.css', array(), '20160816' );

	wp_enqueue_style( 'twentysixteen-info', get_template_directory_uri() . '/css/info.css', array(), '20160816' );


	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'twentysixteen-ie', get_template_directory_uri() . '/css/ie.css', array( 'twentysixteen-style' ), '20160816' );

	wp_style_add_data( 'twentysixteen-ie', 'conditional', 'lt IE 10' );

	// Load the Internet Explorer 8 specific stylesheet.
	wp_enqueue_style( 'twentysixteen-ie8', get_template_directory_uri() . '/css/ie8.css', array( 'twentysixteen-style' ), '20160816' );
	wp_style_add_data( 'twentysixteen-ie8', 'conditional', 'lt IE 9' );

	// Load the Internet Explorer 7 specific stylesheet.
	wp_enqueue_style( 'twentysixteen-ie7', get_template_directory_uri() . '/css/ie7.css', array( 'twentysixteen-style' ), '20160816' );
	wp_style_add_data( 'twentysixteen-ie7', 'conditional', 'lt IE 8' );

	// Load the html5 shiv.
	wp_enqueue_script( 'twentysixteen-html5', get_template_directory_uri() . '/js/html5.js', array(), '3.7.3' );
	wp_script_add_data( 'twentysixteen-html5', 'conditional', 'lt IE 9' );

	wp_enqueue_script( 'twentysixteen-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20160816', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'twentysixteen-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20160816' );
	}

	wp_enqueue_script( 'twentysixteen-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20160816', true );

	wp_localize_script( 'twentysixteen-script', 'screenReaderText', array(
		'expand'   => __( 'expand child menu', 'twentysixteen' ),
		'collapse' => __( 'collapse child menu', 'twentysixteen' ),
	) );


}
add_action( 'wp_enqueue_scripts', 'twentysixteen_scripts' );

/**
 * Adds custom classes to the array of body classes.
 *
 * @since Twenty Sixteen 1.0
 *
 * @param array $classes Classes for the body element.
 * @return array (Maybe) filtered body classes.
 */
function twentysixteen_body_classes( $classes ) {
	// Adds a class of custom-background-image to sites with a custom background image.
	if ( get_background_image() ) {
		$classes[] = 'custom-background-image';
	}

	// Adds a class of group-blog to sites with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of no-sidebar to sites without active sidebar.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'twentysixteen_body_classes' );

/**
 * Converts a HEX value to RGB.
 *
 * @since Twenty Sixteen 1.0
 *
 * @param string $color The original color, in 3- or 6-digit hexadecimal form.
 * @return array Array containing RGB (red, green, and blue) values for the given
 *               HEX code, empty array otherwise.
 */
function twentysixteen_hex2rgb( $color ) {
	$color = trim( $color, '#' );

	if ( strlen( $color ) === 3 ) {
		$r = hexdec( substr( $color, 0, 1 ).substr( $color, 0, 1 ) );
		$g = hexdec( substr( $color, 1, 1 ).substr( $color, 1, 1 ) );
		$b = hexdec( substr( $color, 2, 1 ).substr( $color, 2, 1 ) );
	} else if ( strlen( $color ) === 6 ) {
		$r = hexdec( substr( $color, 0, 2 ) );
		$g = hexdec( substr( $color, 2, 2 ) );
		$b = hexdec( substr( $color, 4, 2 ) );
	} else {
		return array();
	}

	return array( 'red' => $r, 'green' => $g, 'blue' => $b );
}

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for content images
 *
 * @since Twenty Sixteen 1.0
 *
 * @param string $sizes A source size value for use in a 'sizes' attribute.
 * @param array  $size  Image size. Accepts an array of width and height
 *                      values in pixels (in that order).
 * @return string A source size value for use in a content image 'sizes' attribute.
 */
function twentysixteen_content_image_sizes_attr( $sizes, $size ) {
	$width = $size[0];

	if ( 840 <= $width ) {
		$sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 62vw, 840px';
	}

	if ( 'page' === get_post_type() ) {
		if ( 840 > $width ) {
			$sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
		}
	} else {
		if ( 840 > $width && 600 <= $width ) {
			$sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 61vw, (max-width: 1362px) 45vw, 600px';
		} elseif ( 600 > $width ) {
			$sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
		}
	}

	return $sizes;
}
add_filter( 'wp_calculate_image_sizes', 'twentysixteen_content_image_sizes_attr', 10 , 2 );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for post thumbnails
 *
 * @since Twenty Sixteen 1.0
 *
 * @param array $attr Attributes for the image markup.
 * @param int   $attachment Image attachment ID.
 * @param array $size Registered image size or flat array of height and width dimensions.
 * @return array The filtered attributes for the image markup.
 */
function twentysixteen_post_thumbnail_sizes_attr( $attr, $attachment, $size ) {
	if ( 'post-thumbnail' === $size ) {
		if ( is_active_sidebar( 'sidebar-1' ) ) {
			$attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 60vw, (max-width: 1362px) 62vw, 840px';
		} else {
			$attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 88vw, 1200px';
		}
	}
	return $attr;
}
add_filter( 'wp_get_attachment_image_attributes', 'twentysixteen_post_thumbnail_sizes_attr', 10 , 3 );

/**
 * Modifies tag cloud widget arguments to display all tags in the same font size
 * and use list format for better accessibility.
 *
 * @since Twenty Sixteen 1.1
 *
 * @param array $args Arguments for tag cloud widget.
 * @return array The filtered arguments for tag cloud widget.
 */
function twentysixteen_widget_tag_cloud_args( $args ) {
	$args['largest']  = 1;
	$args['smallest'] = 1;
	$args['unit']     = 'em';
	$args['format']   = 'list';

	return $args;
}
add_filter( 'widget_tag_cloud_args', 'twentysixteen_widget_tag_cloud_args' );

add_action( 'after_setup_theme', 'wpdocs_theme_setup' );
function wpdocs_theme_setup() {
    add_image_size( 'product-thumb', 540, 420, true );
    add_image_size( 'news-thumb', 1000, 500, true );
}

add_action( 'admin_menu', 'ddev_menu' );
function ddev_menu() {
	add_menu_page( 'Slider', 'Slider', 'manage_options', 'ddev_settings', 'ddev_func_settings', get_stylesheet_directory_uri('stylesheet_directory')."/img/ico.png");
}

add_action( 'admin_init', 'register_ddev_settings' );
function register_ddev_settings() {
	register_setting( 'ddev-settings-group', 'ddev_settings' );
}

function ddev_func_settings() {
	if ( !current_user_can( 'manage_options' ) ) {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	$ddev_settings = get_option( 'ddev_settings' );
?>
	<div class="wrap">
		<form method="post" action="options.php">
		  <?php settings_fields( 'ddev-settings-group' ); ?>
		  <?php do_settings_sections( 'ddev_settings' ); ?>

		  <h2 class="title">Setting slider</h2>
		  <table class="form-table">
				<tbody>
					<tr>
						<th scope="row">
							<label for="ddev_banner_url">Slider1</label>
							<p class="description" id="ddev_banner_url_description">Loại file: Flash, jpg, png, gif</p>
						</th>

						<td>
							<?php
								if(function_exists( 'wp_enqueue_media' )){
									wp_enqueue_media();
								} else {
									wp_enqueue_style('thickbox');
									wp_enqueue_script('media-upload');
									wp_enqueue_script('thickbox');
								}
							?>
							<input id="ddev_banner_url" class="regular-text ddev_banner_url" name="ddev_settings[ddev_banner_url]" value="<?php echo $ddev_settings['ddev_banner_url']; ?>" type="text">
		   				<input id="ddev_banner_upload" class="button button-primary ddev_banner_upload" name="ddev_settings[ddev_banner_upload]" value="Upload" type="submit">

						</td>
					</tr>
					<tr>
						<th scope="row"><label for="ddev_banner_url"></label></th>
						<td>
							<?php
								$info_banner = new SplFileInfo( $ddev_settings['ddev_banner_url'] );
								$file_type = strtoupper( $info_banner->getExtension() );
							?>
							<?php if($file_type == "SWF") : ?>
								<embed height="100" width="635" type="application/x-shockwave-flash" allowscriptaccess="always" wmode="transparent" allowfullscreen="false" scale="noborder" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" quality="high" src="<?php echo $ddev_settings['ddev_banner_url']; ?>" title="<?php bloginfo('name'); ?>"/>
							<?php else : ?>
								<img class="ddev_banner_url" height="100" src="<?php echo $ddev_settings['ddev_banner_url']; ?>" title="<?php bloginfo('name'); ?>"/>
							<?php endif; ?>
						</td>
					</tr>
					<script>
						jQuery(document).ready(function($) {
							$('.ddev_banner_upload').click(function(e) {
								e.preventDefault();

								var custom_uploader = wp.media({
									title: 'Chọn ảnh',
									button: {
										text: 'Chèn ảnh'
									},
									multiple: false  // Set this to true to allow multiple files to be selected
								})
								.on('select', function() {
									var attachment = custom_uploader.state().get('selection').first().toJSON();
									$('.ddev_banner').attr('src', attachment.url);
									$('.ddev_banner_url').val(attachment.url);
								})
								.open();
							});
						});
					</script>

					<tr>
						<th scope="row">
							<label for="ddev_banner_mobile_url">Slider2</label>
							<p class="description" id="ddev_banner_url_description">Loại file: jpg, png, gif</p>
						</th>
						<td>
							<?php
								if(function_exists( 'wp_enqueue_media' )){
									wp_enqueue_media();
								} else {
									wp_enqueue_style('thickbox');
									wp_enqueue_script('media-upload');
									wp_enqueue_script('thickbox');
								}
							?>
							<input id="ddev_banner_mobile_url" class="regular-text ddev_banner_mobile_url" name="ddev_settings[ddev_banner_mobile_url]" value="<?php echo $ddev_settings['ddev_banner_mobile_url']; ?>" type="text">
		   				<input id="ddev_banner_mobile_upload" class="button button-primary ddev_banner_mobile_upload" name="ddev_settings[ddev_banner_mobile_upload]" value="Upload" type="submit">
						</td>
					</tr>
					<tr>
						<th scope="row"><label for="ddev_banner_mobile"></label></th>
						<td>
							<?php if($ddev_settings['ddev_banner_mobile_url'] != ""){ echo '<img class="ddev_banner_mobile" src="'. $ddev_settings['ddev_banner_mobile_url'] . '" height="100" />'; } ?>
						</td>
					</tr>
					<script>
						jQuery(document).ready(function($) {
							$('.ddev_banner_mobile_upload').click(function(e) {
								e.preventDefault();

								var custom_uploader = wp.media({
									title: 'Chọn ảnh',
									button: {
										text: 'Chèn ảnh'
									},
									multiple: false  // Set this to true to allow multiple files to be selected
								})
								.on('select', function() {
									var attachment = custom_uploader.state().get('selection').first().toJSON();
									$('.ddev_banner_mobile').attr('src', attachment.url);
									$('.ddev_banner_mobile_url').val(attachment.url);
								})
								.open();
							});
						});
					</script>
					<tr>
						<th scope="row">
							<label for="ddev_banner_url_slider3">Slider3</label>
							<p class="description" id="ddev_banner_url_description">Loại file: jpg, png, gif</p>
						</th>
						<td>
							<?php
								if(function_exists( 'wp_enqueue_media' )){
									wp_enqueue_media();
								} else {
									wp_enqueue_style('thickbox');
									wp_enqueue_script('media-upload');
									wp_enqueue_script('thickbox');
								}
							?>
							<input id="ddev_banner_url_slider3" class="regular-text ddev_banner_url_slider3" name="ddev_settings[ddev_banner_url_slider3]" value="<?php echo $ddev_settings['ddev_banner_url_slider3']; ?>" type="text">
							<input id="ddev_banner_upload_slider" class="button button-primary ddev_banner_upload_slider" name="ddev_settings[ddev_banner_upload_slider]" value="Upload" type="submit">
						</td>
					</tr>
					<tr>
						<th scope="row"><label for="ddev_banner_url_slider3"></label></th>
						<td>
							<?php if($ddev_settings['ddev_banner_url_slider3'] != ""){ echo '<img class="ddev_banner_url_slider3" src="'. $ddev_settings['ddev_banner_url_slider3'] . '" height="100" />'; } ?>
						</td>
					</tr>
					<script>
						jQuery(document).ready(function($) {
							$('.ddev_banner_upload_slider').click(function(e) {
								e.preventDefault();

								var custom_uploader = wp.media({
									title: 'Chọn ảnh',
									button: {
										text: 'Chèn ảnh'
									},
									multiple: false  // Set this to true to allow multiple files to be selected
								})
								.on('select', function() {
									var attachment = custom_uploader.state().get('selection').first().toJSON();
									$('.ddev_banner_url_slider3').attr('src', attachment.url);
									$('.ddev_banner_url_slider3').val(attachment.url);
								})
								.open();
							});
						});
					</script>
				</tbody>
			</table>

		  <?php submit_button(); ?>
		</form>
	</div>
<?php
}

add_action('admin_head', 'my_custom_fonts');
function my_custom_fonts() {
	echo "<style>
	input.ddev_banner_url, input.ddev_banner_mobile_url, input.ddev_banner_url_slider3{
		padding: 9px 10px;
		width: 70%;
	}
	.ddev_banner_upload, .ddev_banner_mobile_upload, .ddev_banner_upload_slider{
		height: 40px !important;
	}
	</style>";
}

function php_execute($html){
    if(strpos($html,"<"."?php")!==false){
        ob_start(); eval("?".">".$html);
        $html=ob_get_contents();
        ob_end_clean();
    }
return $html;
}
add_filter('widget_text','php_execute',100);

function ddev_get_setting( $name ) {
	$ddev_settings = get_option( 'ddev_settings' );
	if( !empty( $ddev_settings[$name] ))
		return $ddev_settings[$name];

	return false ;
}


add_action('show_user_profile', 'restrict_user_form');
add_action('edit_user_profile', 'restrict_user_form');
 
function restrict_user_form($user)
{
	$args = array(
		'show_option_all' => '',
		'show_option_none' => '',
		'orderby' => 'ID',
		'order' => 'ASC',
		'show_count' => 0,
		'hide_empty' => 0,
		'child_of' => 0,
		'exclude' => '',
		'echo' => 1,
		'selected' => get_user_meta($user->ID, '_access', true),
		'hierarchical' => 0,
		'name' => 'allow',
		'id' => '',
		'class' => 'postform',
		'depth' => 0,
		'tab_index' => 0,
		'taxonomy' => 'category',
		'hide_if_empty' => false,
		'walker' => ''
	);
	?>
 
	<h3>Restrict Author Post to a category</h3>
	 
	<table>
	<tr>
	<th><label for="access">Writing to:</label></th>
	 
	<td>
	<?php wp_dropdown_categories($args); ?>
	<br />
	<span>Use to restrict an author posting to just one category.</span>
	</td>
	</tr>
	 
	</table>
<?php }
add_action('personal_options_update', 'restrict_save_data');
add_action('edit_user_profile_update', 'restrict_save_data');
 
function restrict_save_data($user_id)
{
	if (!current_user_can('administrator', $user_id))
	return false;
	update_user_meta($user_id, '_access', $_POST['allow']);
}
function is_restrict()
{
	if (get_user_meta(get_current_user_id(), '_access', true) != '')
		return true;
	else
		return false;
}
add_action('edit_form_after_title', 'restrict_warning');
function restrict_warning($post_data = false)
{
	if (is_restrict()) {
		$c    = get_user_meta(get_current_user_id(), '_access', true);
		$data = get_category($c);
		echo 'Bạn chỉ được đăng bài viết vào chuyên mục: <strong>' . $data->name . '</strong><br /><br />';
	}
}

function restrict_remove_meta_boxes()
{
	if (is_restrict())
	remove_meta_box('categorydiv', 'post', 'normal');
}
add_action('admin_menu', 'restrict_remove_meta_boxes');

add_action('save_post', 'save_restrict_post');
function save_restrict_post($post_id)
{
	if (!wp_is_post_revision($post_id) && is_restrict()) {
		remove_action('save_post', 'save_restrict_post');
		wp_set_post_categories($post_id, get_user_meta(get_current_user_id(), '_access', true));
		add_action('save_post', 'save_restrict_post');
	}
}
function ddev_thumb( $w,$h ) {
	global $post;
	$img_customfield = get_post_meta($post->ID, 'thumb', true);
	$img_attachment_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
	$img_uploads = get_children( array('post_parent' => $post->ID, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => 'ASC', 'orderby' => 'menu_order ID', 'numberposts' => 1) );
	$img_post = preg_match_all('/\< *[img][^\>]*src *= *[\"\']{0,1}([^\"\'\ >]*)/',get_the_content(),$matches);
	$img_default = get_template_directory_uri().'/images/no-thumb.png';

	@define( BFITHUMB_UPLOAD_DIR, 'Thumbcache' );	
	$params = array( 'width' => $w, 'height' => $h );

	// get thumbnail
	if ($img_customfield) {
		echo '<img src="'. bfi_thumb( $img_customfield, $params ) .'" alt="'.get_the_title($post).'" title="'.get_the_title($post).'" />';
	} elseif ($img_attachment_image) {
		echo '<img src="'. bfi_thumb( $img_attachment_image[0], $params ) .'" alt="'.get_the_title($post).'" title="'.get_the_title($post).'" />';
	} elseif ($img_uploads == true) {
		foreach($img_uploads as $id => $attachment) {
			$img = wp_get_attachment_image_src($id, 'full');
			echo '<img src="'. bfi_thumb( $img[0], $params ) .'" alt="'.get_the_title($post).'" title="'.get_the_title($post).'" />';
		}
	} elseif (count($matches[1]) > 0) {
		echo '<img src="'. bfi_thumb( $matches[1][0], $params ) .'" alt="'.get_the_title($post).'" title="'.get_the_title($post).'" />';
	} else {
		echo '<img src="'. bfi_thumb( $img_default, $params ) .'" alt="'.get_the_title($post).'" title="'.get_the_title($post).'" />';
	}
}
function ddev_excerpt( $limit ) {
	$excerpt = explode(' ', get_the_excerpt(), $limit);
	if (count($excerpt) >= $limit) {
		array_pop($excerpt);
		$excerpt = implode(" ",$excerpt);
	} else {
		$excerpt = implode(" ",$excerpt);
	}
	$excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
	return $excerpt;
}
function ddev_pagenavi( $query = false, $num = false ){
	echo '<div class="ddev-pagenavi">';	
		ddev_get_pagenavi( $query, $num );
	echo '</div>';
}




