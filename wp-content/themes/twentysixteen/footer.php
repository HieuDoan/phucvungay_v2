
</div><!-- .site-content -->
<div class="top-footer" role="contentinfo">
	<div class="container">
		<div class="row">
			<div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-12">
				<a href=""><img src="<?php bloginfo('template_url'); ?>/img/footer_logo.png" class="footer-logo" alt="Bất động sản" ></a>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 product-info">
				<ul class="">
					<li class="first">
						Công ty cổ phần dịch vụ<br> thương mại điện tử<br> Fserving Việt Nam
					</li>
					<li>
						Địa chỉ: Số 19 đường Ông <br>Ích Khiêm, Quận Hải Châu,<br> Thành phố Đà Nẵng
					</li>
					<li>
						Hotline: (0236) 333 333<br>
						Email: info@fserving.com
					</li>
					<li>
				</ul>
			</div>
			<div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 product-link">
				<ul class="">
					<li>
						<a href="">Thông tin mới </a>
					</li>
					<li>
						<a href="">Hỗ trợ</a>
					</li>
					<li>
						<a href="">Quảng cáo</a>
					</li>
					<li>
						<a href="">Các điều khoản</a>
					</li>
					<li>
						<a href="">Tìm kiếm </a>
					</li>
					<li>
				</ul>
			</div>
			<div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 product-link">
				<ul class="">
					<li>
						<a href="">Về chúng tôi </a>
					</li>
					<li>
						<a href="">Bản quyền	</a>
					</li>
					<li>
						<a href="">Đối tác</a>
					</li>
					<li>
						<a href="">Tuyển dụng</a>
					</li>
					<li>
						<a href="">Liên hệ</a>
					</li>
					<li>
				</ul>
			</div>
			<div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 product-service">
				<h4>Các dịch vụ</h4>
				<ul class="lít-service">
					<li><a href="">FSERVING HOME</a></li>
					<li><a href="">FSERVING MARKET</a></li>
					<li><a href="">FSERVING BUILDING</a></li>
					<li><a href="">FSERVING CITY</a></li>
				</ul>
			</div>
		</div>
		<div class="row margin-sercurity">
			<div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
				<img src="<?php bloginfo('template_url'); ?>/img/sercurity.png">
			</div>
			<div class="col-xl-9 col-lg-9 col-md-6 col-sm-6 col-12">
				<ul class="list-inline text-social">
					<li class="list-inline-item"><a href=""><img src="<?php bloginfo('template_url'); ?>/img/fb.png"></a></li>
					<li class="list-inline-item"><a href=""><img src="<?php bloginfo('template_url'); ?>/img/ig.png"></a></li>
				</ul>
			</div>
		</div>
		<p class="copyright">
			@ 2018 FSERVING COMPANY. ALL RIGHT RESERVED
		</p>
	</div><!-- .site -->
	<div class="hidden-xs hidden-sm toolbox" id="toolbox">
		<ul class="list-group">
			<li>
				<i class="fab fa-facebook-square"></i>
			</li>
		</ul>
		<i class="fas fa-caret-right"></i>
	</div>
	<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FPh%E1%BB%A5c-v%E1%BB%A5-ngay-KL-1531048193678415%2F&tabs=timeline&width=340&height=350&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=1719217484776342" width="340" height="300" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media" class="iframe"></iframe>
	<?php wp_footer(); ?>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
	<script type="text/javascript">
		$("#toolbox").click(function(){
			if($(".iframe").hasClass("show_frame")){
				$(".iframe").removeClass("show_frame");
			}
			else{
				$(".iframe").addClass("show_frame");
			}
		});
		$('#menu-toggle').click(function(){
			$('#site-header-menu #site-navigation').toggleClass('show_menu');
		});
	</script>
</body>
</html>
