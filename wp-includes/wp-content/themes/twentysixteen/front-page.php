<?php
get_header(); ?>
<div id="primary" class="">
	<main id="main" class="site-main" role="main">
		<?php
			get_template_part( 'template-parts/common/content', 'slider' );
			get_template_part( 'template-parts/page/content', 'information' );
			get_template_part( 'template-parts/page/content', 'process' );
			get_template_part( 'template-parts/common/content', 'product' );
			get_template_part( 'template-parts/page/content', 'team' );
			get_template_part( 'template-parts/page/content', 'partner' );
			get_template_part( 'template-parts/page/content', 'contact' );
		?>
	</main>
</div>
<?php get_footer();
