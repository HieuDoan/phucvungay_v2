<?php
/********************************************************************
// Widget team
********************************************************************/
class team_widget extends WP_Widget {

  # Constructor
  function team_widget() {
    $widget_ops = array('description' => 'team');
    $this->WP_Widget('team_widget', '+ team', $widget_ops);
  }

  # Display Widget
  function widget($args, $instance) {
    extract($args);
    
    # Instances
    $showpost_number = $instance['showpost_number'];
    $showpost_title = $instance['showpost_title'];
    $showpost_des = $instance['showpost_des'];
    
    # Display
    ?>
    <div class="top-team">
        <h2 class="title-team"><?php echo $showpost_title; ?></h2>
        <p class="title-team"><?php echo $showpost_des; ?></p>
    </div>
    <div class="bottom-team">
      <div class="container">
        <div class="w-content team-content">
            
         
            <ul class="row">
              <?php
 
                  
                $args = array ( 'post_status' => 'publish',
                                'post_type' => "team",
                                'showposts' => $showpost_number,
                              );
              ?>
              <?php $getposts = new WP_query($args); ?>
              <?php if ($getposts->have_posts()) : ?>
                <?php while ($getposts->have_posts()) : $getposts->the_post(); ?>
                  <li class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                      <?php the_post_thumbnail( 'team-thumb' ); ?>
                      <div class="title_main">
                          <h4>
                              <?php echo types_render_field('ten', array('output' => 'raw')); ?>
                          </h4>
                          <div>
                              <ul>
                                <li><a href="<?php echo types_render_field('instagram', array('output' => 'raw')); ?>"><i class="fab fa-instagram"></i></a></li>
                                <li><a href="<?php echo types_render_field('twitter', array('output' => 'raw')); ?>"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="<?php echo types_render_field('facebook', array('output' => 'raw')); ?>"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="<?php echo types_render_field('skype', array('output' => 'raw')); ?>"><i class="fab fa-skype"></i></a></li>
                              </ul>
                          </div>
                      </div>
                  </li>
                <?php endwhile; ?>
              <?php endif; ?>
            </ul>
          </div>
      </div>
    </div><!--End .team-->
    
    <?php
  }

  # Update form
  function update($new_instance, $old_instance) {
    if (!isset($new_instance['submit'])) {
      return false;
    }
    # Instances (old = new)
    $instance = $old_instance;
    $instance['showpost_number'] = $new_instance['showpost_number'];
    $instance['showpost_title'] = $new_instance['showpost_title'];
    $instance['showpost_des'] = $new_instance['showpost_des'];
    return $instance;
  }

  # Options form
  function form($instance) {
    global $wpdb;
    # Instances
    $instance = wp_parse_args((array) $instance, array('title' => ''));
    $showpost_number = $instance['showpost_number'];
    $showpost_title = $instance['showpost_title'];
    $showpost_des = $instance['showpost_des'];
?>
<p>
  <label for="<?php echo $this->get_field_id('showpost_title'); ?>">Tiêu đề:</label>
  <input class="widefat" id="<?php echo $this->get_field_id('showpost_title'); ?>" name="<?php echo $this->get_field_name('showpost_title'); ?>" type="text" value="<?php echo $showpost_title; ?>" />
</p>
<p>
  <label for="<?php echo $this->get_field_id('showpost_des'); ?>">Mô tả:</label>
  <textarea rows="5" class="widefat" id="<?php echo $this->get_field_id('showpost_des'); ?>" name="<?php echo $this->get_field_name('showpost_des'); ?>"><?php echo $showpost_des; ?></textarea>
</p>
<p>
  <label for="<?php echo $this->get_field_id('showpost_number'); ?>">Số bài viết:</label>
  <input class="widefat" id="<?php echo $this->get_field_id('showpost_number'); ?>" name="<?php echo $this->get_field_name('showpost_number'); ?>" type="text" value="<?php echo $showpost_number; ?>" />
</p>

<input type="hidden" id="<?php echo $this->get_field_id('submit'); ?>" name="<?php echo $this->get_field_name('submit'); ?>" value="1" />

<?php
  }
}

### Initiate widget
add_action('widgets_init', 'team_widget_box');
function team_widget_box() {
  register_widget('team_widget');
}




?>