<?php
/********************************************************************
// Widget partner
********************************************************************/
class partner_widget extends WP_Widget {

  # Constructor
  function partner_widget() {
    $widget_ops = array('description' => 'partner');
    $this->WP_Widget('partner_widget', '+ partner', $widget_ops);
  }

  # Display Widget
  function widget($args, $instance) {
    extract($args);
    
    # Instances
    $showpost_number = $instance['showpost_number'];
    $showpost_title = $instance['showpost_title'];
    $showpost_des = $instance['showpost_des'];
    
    # Display
    ?>
    
    <div class="partner row">
      <div class="w-content partner-content">
        <div class="mar-partner">
          <h2 class="title-partner"><?php echo $showpost_title; ?></h2>
          <p class="">
            <?php echo $showpost_des; ?>
          </p>
        </div>
        
        <ul class="row">
          <?php
            $args = array ( 'post_status' => 'publish',
                            'post_type' => "doi-tac",
                            'showposts' => $showpost_number,
                          );
          ?>
          <?php $getposts = new WP_query($args); ?>
          <?php if ($getposts->have_posts()) : ?>
            <?php while ($getposts->have_posts()) : $getposts->the_post(); ?>
              <li class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-6">
                  <?php the_post_thumbnail( 'partner-thumb' ); ?>
                  <div>
                      <h4>
                        <?php echo types_render_field('ho-ten', array('output' => 'raw')); ?>
                      </h4>
                      <p class="office">
                        <span>Đơn vi:</span> <?php echo types_render_field('ho-ten', array('output' => 'raw')); ?><br>
                        <span>Phone:</span> <?php echo types_render_field('phone', array('output' => 'raw')); ?><br>
                        <span>Nhận xét:</span> <?php echo types_render_field('nhan-xet', array('output' => 'raw')); ?>
                      </p>
                  </div>
              </li>
            <?php endwhile; ?>
          <?php endif; ?>
        </ul>
      </div>
    </div><!--End .partner-->
    
    <?php
  }

  # Update form
  function update($new_instance, $old_instance) {
    if (!isset($new_instance['submit'])) {
      return false;
    }
    # Instances (old = new)
    $instance = $old_instance;
    $instance['showpost_number'] = $new_instance['showpost_number'];
    $instance['showpost_title'] = $new_instance['showpost_title'];
    $instance['showpost_des'] = $new_instance['showpost_des'];
    return $instance;
  }

  # Options form
  function form($instance) {
    global $wpdb;
    # Instances
    $instance = wp_parse_args((array) $instance, array('title' => ''));
    $showpost_number = $instance['showpost_number'];
    $showpost_title = $instance['showpost_title'];
    $showpost_des = $instance['showpost_des'];
?>
<p>
  <label for="<?php echo $this->get_field_id('showpost_title'); ?>">Tiêu đề:</label>
  <input class="widefat" id="<?php echo $this->get_field_id('showpost_title'); ?>" name="<?php echo $this->get_field_name('showpost_title'); ?>" type="text" value="<?php echo $showpost_title; ?>" />
</p>
<p>
  <label for="<?php echo $this->get_field_id('showpost_des'); ?>">Mô tả:</label>
  <textarea rows="5" class="widefat" id="<?php echo $this->get_field_id('showpost_des'); ?>" name="<?php echo $this->get_field_name('showpost_des'); ?>"><?php echo $showpost_des; ?></textarea>
</p>

<p>
  <label for="<?php echo $this->get_field_id('showpost_number'); ?>">Số bài viết:</label>
  <input class="widefat" id="<?php echo $this->get_field_id('showpost_number'); ?>" name="<?php echo $this->get_field_name('showpost_number'); ?>" type="text" value="<?php echo $showpost_number; ?>" />
</p>

<input type="hidden" id="<?php echo $this->get_field_id('submit'); ?>" name="<?php echo $this->get_field_name('submit'); ?>" value="1" />

<?php
  }
}

### Initiate widget
add_action('widgets_init', 'partner_widget_box');
function partner_widget_box() {
  register_widget('partner_widget');
}




?>